load "$BATS_HELPER_PATH/bats-support/load.bash"
load "$BATS_HELPER_PATH/bats-assert/load.bash"

setup() {
    # shellcheck disable=SC1091
    . shellmock
    echo '' > "$BATS_TMPDIR/qute_fifo"
}

teardown() {
    if [ -z "$TEST_FUNCTION" ]; then
        shellmock_clean
    fi

    echo '' > "$BATS_TMPDIR/qute_fifo"
}

@test "it errors out if bookie returns a non zero status and it sends an error message to the qutebrowser fifo" {
    # GIVEN
    export QUTE_URL="https://caniuse.com"
    export QUTE_FIFO="$BATS_TMPDIR/qute_fifo"
    export QUTE_TITLE="Can I use"

    # EXPECT
    shellmock_expect bookie \
        --match 'add https://caniuse.com --title Can I use' \
        --output 'Url https://caniuse.com already exists in db with id 1' \
        --status 1

    # WHEN
    run bash userscripts/bookie

    # THEN
    [ "$status" -eq 1 ]
    assert_equal "$(cat "$QUTE_FIFO" | tr -d "\n")" "message-error 'url https://caniuse.com could not be bookmarked. Error was Url https://caniuse.com already exists in db with id 1'"
}

@test "it sends a info message to the qutebrowser fifo it bookmark was added" {
export TEST_FUNCTION=bou
    # GIVEN
    export QUTE_URL="https://caniuse.com"
    export QUTE_FIFO="$BATS_TMPDIR/qute_fifo"
    export QUTE_TITLE="Can I use"

    # EXPECT
    shellmock_expect bookie \
        --match 'add https://caniuse.com --title Can I use' \
        --status 0

    # WHEN
    run bash userscripts/bookie

    # THEN
    [ "$status" -eq 0 ]
    assert_equal "$(cat "$QUTE_FIFO" | tr -d "\n")" "message-info 'url https://caniuse.com was bookmarked !'"
}
