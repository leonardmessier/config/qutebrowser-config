include .env
export

.DEFAULT_GOAL := all

all: lint test

lint: lint-userscripts
lint-userscripts: ## Lint userscripts with shellcheck
	@docker run -v `pwd`:/mnt koalaman/shellcheck userscripts/*
	@echo "\033[32m✓\033[0m User scripts linted with success"
test: test-userscripts
test-userscripts: ## Test qutebrowser userscripts with bats
	@docker-compose run --rm bats tests/bats
	@echo "\033[32m✓\033[0m User scripts tested with success"

